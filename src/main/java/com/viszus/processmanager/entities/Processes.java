package com.viszus.processmanager.entities;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Processes entity, contains list of process information.
 */
@JacksonXmlRootElement(localName = "processes")
public class Processes {

    @JacksonXmlProperty(localName = "process")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<ProcessAttributes> processesList = new ArrayList<>();

    private Processes() {
    }

    public Processes(List<ProcessAttributes> processesList) {
        this.processesList = processesList;
    }

    public List<ProcessAttributes> getProcessesList() {
        return processesList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Processes processes = (Processes) o;

        return processesList != null ? processesList.equals(processes.processesList) : processes.processesList == null;
    }

    @Override
    public int hashCode() {
        return processesList != null ? processesList.hashCode() : 0;
    }
}
