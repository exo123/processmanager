package com.viszus.processmanager.entities;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * ProcessAttributes entity, contains name of process, working directory of process
 * and command which should start the process.
 */
@JacksonXmlRootElement(localName = "process")
public class ProcessAttributes {

    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    @JacksonXmlProperty(localName = "workdir")
    private String workingDirectory;

    @JacksonXmlProperty(localName = "command")
    private String command;

    private ProcessAttributes() {
    }

    public ProcessAttributes(String name, String workingDirectory, String command) {
        this.name = name;
        this.workingDirectory = workingDirectory;
        this.command = command;
    }

    public String getName() {
        return name;
    }

    public String getWorkingDirectory() {
        return workingDirectory;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProcessAttributes that = (ProcessAttributes) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (workingDirectory != null ? !workingDirectory.equals(that.workingDirectory) : that.workingDirectory != null)
            return false;
        return command != null ? command.equals(that.command) : that.command == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (workingDirectory != null ? workingDirectory.hashCode() : 0);
        result = 31 * result + (command != null ? command.hashCode() : 0);
        return result;
    }
}
