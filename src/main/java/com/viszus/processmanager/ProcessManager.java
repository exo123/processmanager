package com.viszus.processmanager;

import com.viszus.processmanager.entities.Processes;
import com.viszus.processmanager.handlers.ProcessHandler;
import com.viszus.processmanager.utilities.ProcessManagerUtilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProcessManager {

    public static void main(final String[] args) {
        try {
            ProcessManagerUtilities.validatedInputArguments(args);
            final Processes processes = ProcessManagerUtilities.unmarshalXml(args[0]);
            if (!processes.getProcessesList().isEmpty()) {
                manageProcesses(processes);
            } else {
                System.out.println("No processes to run, exiting program...");
            }
        } catch (IllegalArgumentException | IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private static void manageProcesses(final Processes processes) throws IOException {
        final AtomicBoolean isRunning = new AtomicBoolean(true);

        //create fixed thread pool with same size as the number of processes
        //every process is handled by its own thread
        final ExecutorService threadPool = Executors.newFixedThreadPool(processes.getProcessesList().size());
        processes.getProcessesList().forEach(processAttributes ->
                threadPool.execute(new ProcessHandler(processAttributes, isRunning)));

        System.out.println("press ENTER to exit...");
        try(final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            reader.readLine();
        } finally {
            System.out.println("Exiting program...");
            //let to know to other threads that the program is exiting
            isRunning.set(false);
            //send interrupt signal to every thread in pool and shutdown the executor
            threadPool.shutdownNow();
        }
    }
}
