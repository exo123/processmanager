package com.viszus.processmanager.handlers;

import com.viszus.processmanager.entities.ProcessAttributes;
import com.viszus.processmanager.utilities.ProcessManagerUtilities;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProcessHandler implements Runnable {

    private enum PrintedMessageType {
        STARTED, ENDED, ENDED_WITH_RESTART, ERROR_WITH_RETRY, ERROR_LAST_RETRY, SUPPORT_NORMAL_TERMINATION
    }

    private static final int RESTART_TIME_IN_MILLIS = 1000;
    //values <= 1: thread will make only one attempt to start process
    private static final int NUMBER_OF_RETRY_ATTEMPTS = 5;

    private final AtomicBoolean isRunning;
    private final ProcessAttributes processAttributes;
    private Process process = null;

    public ProcessHandler(ProcessAttributes processAttributes, AtomicBoolean isRunning) {
        this.processAttributes = processAttributes;
        this.isRunning = isRunning;
    }

    public Process getProcess() {
        return process;
    }

    @Override
    public void run() {
        handleProcess();
    }

    private void handleProcess() {
        //create process builder with command and arguments, set the working directory
        final ProcessBuilder pb = new ProcessBuilder(
                ProcessManagerUtilities.parseCommand(processAttributes.getCommand()));
        pb.directory(new File(processAttributes.getWorkingDirectory()));

        int retryCounter = 1;

        while (isRunning.get()) {
            //trying to start process, if operation is not successful retry after RESTART_TIME_IN_MILLIS limit
            //when the NUMBER_OF_RETRY_ATTEMPTS is reached out, thread is finished
            try {
                process = pb.start();
                printProcessInfoMessage(PrintedMessageType.STARTED);
                //reset retry counter after successful start
                retryCounter = 1;
            } catch (IOException e) {
                if (retryCounter < NUMBER_OF_RETRY_ATTEMPTS) {
                    printProcessErrorMessage(retryCounter++, e.getMessage(), PrintedMessageType.ERROR_WITH_RETRY);
                } else {
                    printProcessErrorMessage(retryCounter, e.getMessage(), PrintedMessageType.ERROR_LAST_RETRY);
                    break;
                }
            }

            // waiting for the termination of process if it is alive, otherwise sleep
            // waiting or sleeping can be interrupted, then the termination logic is started
            try {
                if (isProcessAlive()) {
                    process.waitFor();
                    printProcessInfoMessage(PrintedMessageType.ENDED_WITH_RESTART);
                }

                Thread.sleep(RESTART_TIME_IN_MILLIS);
            } catch (InterruptedException e) {
                if (isProcessAlive()) {
                    terminateProcess();
                }
            }
        }
    }

    private void terminateProcess() {
        printProcessInfoMessage(PrintedMessageType.SUPPORT_NORMAL_TERMINATION);
        process.destroy();
        try {
            //if process supports normal termination, it could take some time to clean up and terminate
            process.waitFor(5, TimeUnit.SECONDS);
            printProcessInfoMessage(PrintedMessageType.ENDED);
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
        }
    }

    private boolean isProcessAlive() {
        return process != null && process.isAlive();
    }

    private void printProcessErrorMessage(final int attempt, final String message,
                                          final PrintedMessageType messageType) {
        switch (messageType) {
            case ERROR_WITH_RETRY:
                System.err.printf("%s - ERROR: (%d. ATTEMPT FROM %d) Process cannot be started,"
                                + " will be retried after " + RESTART_TIME_IN_MILLIS + "ms: %s\n",
                        processAttributes.getName(), attempt, NUMBER_OF_RETRY_ATTEMPTS, message);
                break;
            case ERROR_LAST_RETRY:
                System.err.printf("%s - ERROR: (%d. ATTEMPT - LAST) Process cannot be started,"
                        + " end of retrying: %s\n", processAttributes.getName(), attempt, message);
                break;
            default:
                System.err.printf("Message type %s not supported\n", messageType.toString());
        }
    }

    private void printProcessInfoMessage(final PrintedMessageType messageType) {
        switch (messageType) {
            case STARTED:
                System.out.printf("%s - Process was STARTED - PID: %d\n", processAttributes.getName(), process.pid());
                break;
            case ENDED:
                System.out.printf("%s - Process was ENDED - PID: %d, EXIT CODE: %d\n",
                        processAttributes.getName(), process.pid(), process.exitValue());
                break;
            case ENDED_WITH_RESTART:
                System.out.printf("%s - Process was ENDED and will be restarted after "
                        + RESTART_TIME_IN_MILLIS + "ms - PID: %d, EXIT CODE: %d\n",
                        processAttributes.getName(), process.pid(), process.exitValue());
                break;
            case SUPPORT_NORMAL_TERMINATION:
                System.out.printf("%s - Support normal termination: %B - PID: %d\n",
                        processAttributes.getName(), process.supportsNormalTermination(), process.pid());
                break;
            default:
                System.err.printf("Message type %s not supported\n", messageType.toString());
        }
    }
}
