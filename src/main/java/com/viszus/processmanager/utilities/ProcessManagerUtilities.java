package com.viszus.processmanager.utilities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.viszus.processmanager.entities.Processes;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility methods for process manager.
 */
public final class ProcessManagerUtilities {

    private ProcessManagerUtilities() {
    }

    /**
     * Validation method for program input arguments. Program should be started with exactly one argument.
     * In case of invalid arguments, {@link IllegalArgumentException} is thrown.
     *
     * @param args array of arguments which came to program
     * @throws IllegalArgumentException
     */
    public static void validatedInputArguments(String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            throw new IllegalArgumentException("Error: Exactly 1 argument required.");
        }
    }

    /**
     * This method unmarshal XML file into entities. In case of error {@link IOException} is thrown.
     *
     * @param path path to the XML file
     * @return object Processes which contains the list of process attributes which were read from XML file
     * @throws IOException
     */
    public static Processes unmarshalXml(final String path) throws IOException {
        final File file = new File(path);
        final ObjectMapper mapper = new XmlMapper();
        return mapper.readValue(file, Processes.class);
    }

    /**
     * Method which convert string command with arguments into the list.
     * Example: command "cmd /c "echo Hello World > hello.txt""
     * is parsed to following list ["cmd", "/c", ""echo Hello World > hello.txt""].
     *
     * @param command unparsed string of command and attributes
     * @return list which contains command and attributes
     */
    public static List<String> parseCommand(final String command) {
        final List<String> commandList = new ArrayList<>();

        List<String> splittedCommand = Arrays.asList(command.trim()
                .replaceAll("\\s\"", " \"\"")
                .replaceAll("\"\\s", "\"\" ")
                .split("(\"\\s\")|(\\s\")|(\"\\s)"));

        for (int i = 0; i < splittedCommand.size(); i++) {
            String s = splittedCommand.get(i);

            //if: parts with nested quotation marks,
            //    max the second level of nesting on edges(e.g. "dir "VirtualBox VMs" "Documents"")
            //elseif: parts without quotation marks (e.g. cmd -c)
            //else: parts with simple quotation marks(e.g. "echo Hello World > hello.txt")
            if ((s.startsWith("\"") && !s.endsWith("\"")) || s.startsWith("\"\"")) {
                for (int j = i + 1; j < splittedCommand.size(); j++) {
                    s = splittedCommand.get(j);
                    if ((!s.startsWith("\"") && s.endsWith("\"")) || s.endsWith("\"\"")) {
                        commandList.add(String.join(" ", splittedCommand.subList(i, j + 1)));
                        i = j+1;
                    }
                }
            } else if (!s.contains("\"")) {
                commandList.addAll(Arrays.asList(s.split("\\s+")));
            } else {
                commandList.add(s);
            }
        }

        return commandList;
    }
}
