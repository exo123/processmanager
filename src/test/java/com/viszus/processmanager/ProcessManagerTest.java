package com.viszus.processmanager;

import com.viszus.processmanager.entities.ProcessAttributes;
import com.viszus.processmanager.entities.Processes;
import com.viszus.processmanager.handlers.ProcessHandler;
import com.viszus.processmanager.utilities.ProcessManagerUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

public class ProcessManagerTest {

    @ParameterizedTest
    @MethodSource("parsedCommandProvider")
    public void commandParserParametrizedTest(final String command, List<String> parsedCommand) {
        final List<String> result = ProcessManagerUtilities.parseCommand(command);
        Assertions.assertEquals(parsedCommand, result);
    }

    @Test
    public void unmarshalXMLTest() throws IOException {
        final List<ProcessAttributes> processAttributesList = Arrays.asList(
                new ProcessAttributes("processA", "C:\\Users\\Exo",
                        "cmd /c \"echo Hello World > hello.txt\""),
                new ProcessAttributes("processB", "C:\\Users\\Exo",
                        "notepad \"netstat.txt\""));

        final Processes processes = new Processes(processAttributesList);
        final Processes result = ProcessManagerUtilities.unmarshalXml("src/test/resources/processes.xml");

        Assertions.assertEquals(result, processes);
    }

    @EnabledOnOs(OS.WINDOWS)
    @Test
    public void processWasStartedWindowsTest() throws InterruptedException {
        processWasStartedTest("java --version", "C:\\");
    }

    @EnabledOnOs(OS.LINUX)
    @Test
    public void processWasStartedLinuxTest() throws InterruptedException {
        processWasStartedTest("java --version", "/home/");

    }

    private void processWasStartedTest(final String command, final String directory) throws InterruptedException {
        final AtomicBoolean isRunning = new AtomicBoolean(true);
        final ProcessAttributes processAttributes = new ProcessAttributes("process A", directory, command);
        final ProcessHandler handler = new ProcessHandler(processAttributes, isRunning);
        final Thread workerThread = new Thread(handler);
        workerThread.start();
        Thread.sleep(1000);
        isRunning.set(false);
        workerThread.interrupt();
        Assertions.assertTrue(handler.getProcess() != null && handler.getProcess().pid() >= 0);
    }

    private static Stream<Arguments> parsedCommandProvider() {
        return Stream.of(
                Arguments.of(
                        "cmd /c \"echo Hello World > hello.txt\"",
                        Arrays.asList("cmd", "/c", "\"echo Hello World > hello.txt\"")),
                Arguments.of(
                        "/bin/sh -c \"echo Hello World > hello.txt\"",
                        Arrays.asList("/bin/sh", "-c", "\"echo Hello World > hello.txt\"")),
                Arguments.of(
                        "greeter.sh -c 1",
                        Arrays.asList("greeter.sh", "-c", "1")),
                Arguments.of(
                        "bash -c \"ls -l | grep \"\\.txt$\"\"",
                        Arrays.asList("bash", "-c", "\"ls -l | grep \"\\.txt$\"\"")),
                Arguments.of(
                        "cmd /c \"dir \"VirtualBox VMs\" Documents\"",
                        Arrays.asList("cmd", "/c", "\"dir \"VirtualBox VMs\" Documents\"")),
                Arguments.of(
                        "cmd /c \"dir \"VirtualBox VMs\" \"Documents\"\"",
                        Arrays.asList("cmd", "/c", "\"dir \"VirtualBox VMs\" \"Documents\"\"")),
                Arguments.of(
                        "cmd /c \"\"VirtualBox VMs\" \"Documents\"\"",
                        Arrays.asList("cmd", "/c", "\"\"VirtualBox VMs\" \"Documents\"\"")),
                Arguments.of(
                        "cmd /c \"echo Hello World > hello.txt\" \"echo Hello2 World > hello2.txt\"",
                        Arrays.asList("cmd", "/c", "\"echo Hello World > hello.txt\"",
                                "\"echo Hello2 World > hello2.txt\""))
        );
    }
}
